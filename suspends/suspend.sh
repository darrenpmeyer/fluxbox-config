#!/bin/sh

echo "$(date +%Y-%m-%dT%H:%M:%S) Suspend Requested" >> /var/log/suspend.log
systemctl suspend
echo "$(date +%Y-%m-%dT%H:%M:%S) Suspend handler finished" >> /var/log/suspend.log

