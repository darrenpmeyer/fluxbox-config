# ACPI configuration

On my mSpin3 (Acer Spin3), I use a systemd-suspend script to launch Xscreensaver to lock the screen on suspend. To do that, I need to modify some things. There are scripts here to link

* put `pre-suspend.sh` in `/usr/lib/systemd/system-sleep/` and set it executable. This will run whenever systemd's suspend target runs
* link `request-sleep-lid` into `/etc/acpi/events` and `suspend.sh` (set +x)  into `/etc/acpi`. This will cause the lid-close event to trigger systemd's suspend target. You may need to `sudo systemctl restart acpid`
* make sure that `xscreensaver -no-splash &` appears in `~/.fluxbox/startup` so that the above can talk to it to request a lock

Exit and restart fluxbox and/or reboot the machine
